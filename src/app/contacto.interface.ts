export interface Contacto {
  nombre : string;
  numero : number;
  correo : string;
}
