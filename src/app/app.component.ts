import { Component } from '@angular/core';

import { Contacto } from './contacto';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'agendaTelefonica';
  registroActual : any;
  registroPosicion : any;
  nuevoRegistro : any;

  agregar(oRes){
    let oAux = JSON.stringify(oRes);
    oAux = JSON.parse(oAux);
    this.nuevoRegistro = oAux;
    this.registroActual = { };
  }

  modificarContacto(oData){
    let oAux = JSON.stringify(oData);
    oAux = JSON.parse(oAux);

    this.registroActual = oAux.contacto;
    this.registroPosicion = oAux.pos;
  }

  limpiarRegisro(){
    this.registroActual = { };
    this.registroPosicion = null:
  }
}
