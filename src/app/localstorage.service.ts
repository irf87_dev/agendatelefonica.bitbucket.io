import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {
  oContactos : any;

  constructor() {
    this.oContactos = this.get();
  }

  get(){
    let aux = localStorage.getItem("data");
    if(!aux) localStorage.setItem("data","[]");
    else aux = JSON.parse(aux);
    return aux;
  }

  set(data){
    localStorage.setItem("data",JSON.stringify(data));
  }

}
