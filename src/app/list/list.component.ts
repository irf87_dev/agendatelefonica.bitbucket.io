import { Component, OnInit, OnChange, Input, Output, EventEmitter } from '@angular/core';

import { Contacto } from '../contacto.interface';
import { LocalstorageService } from '../localstorage.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [LocalstorageService]
})
export class ListComponent implements OnInit {
  contactsList : Contacto[] = [];

  @Input() "nuevoContacto" : any;
  @Output() "modificar" = new EventEmitter();
  @Output() "limpiar" = new EventEmitter();

  constructor(
    private storage : LocalstorageService
  ) { }

  ngOnInit() {
    this.contactsList = this.storage.get();
  }

  ngOnChanges(changes){
    if(changes.nuevoContacto){
      if(changes.nuevoContacto.currentValue){
        let arrayKeys = Object.keys(changes.nuevoContacto.currentValue);
        if(arrayKeys.length > 0){
          this.insertar(changes.nuevoContacto.currentValue);
        }
      }
    }
  }

  editar(data,pos){
    this.modificar.emit({ contacto : data , pos : pos });
  }

  eliminar(pos){
    this.contactsList.splice(pos,1);
    this.storage.set(this.contactsList);
  }

  insertar(oData){
    if((typeof oData.position ) == "number"){
      this.contactsList[oData.position] = oData.data;
    }
    else{
      this.contactsList.push(oData.data);
    }
    this.storage.set(this.contactsList);
    setTimeout(()=>{
      this.limpiar.emit();
    },150);
  }

}
