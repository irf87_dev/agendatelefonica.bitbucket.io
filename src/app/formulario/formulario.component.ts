import { Component, OnInit, OnChanges, Input, Output, EventEmitter , ViewChild } from '@angular/core';
//import {FormControl, Validators} from '@angular/forms';
import { Contacto } from '../contacto';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  @ViewChild('modalForm') public modalForm;

  @Input() "data" : any;
  @Input() "position" : any;
  @Output() "enviar" = new EventEmitter();
  contacto : Contacto;

  constructor() {
    this.contacto = new Contacto();
  }

  ngOnInit() {
  }

  ngOnChanges(changes){
    if(changes.data){
      if(changes.data.currentValue){
        if(changes.data.currentValue.nombre){
          this.contacto = changes.data.currentValue;
        }
        else this.contacto = new Contacto();
      }
    }
  }

  aplicar(){
    let oAux = JSON.stringify(this.contacto);
    oAux = JSON.parse(oAux);

    let oResp = {
      data : oAux,
      position : this.position
    }
    this.enviar.emit(oResp);
    this.modalForm.resetForm({});
  }

}
