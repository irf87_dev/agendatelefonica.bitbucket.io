import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'showError'
})
export class ShowErrorPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    try{
      if(value.email) value = "Correo no valido";
      if(value.required) value = "Campo requerido";
    }
    catch(err){
      value = "";
      console.error(err);
    }
    return value;
  }

}
